﻿namespace CalCool
{
    partial class CalCool
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CalCool));
            this.but1 = new System.Windows.Forms.Button();
            this.but2 = new System.Windows.Forms.Button();
            this.but3 = new System.Windows.Forms.Button();
            this.but4 = new System.Windows.Forms.Button();
            this.but5 = new System.Windows.Forms.Button();
            this.but6 = new System.Windows.Forms.Button();
            this.but7 = new System.Windows.Forms.Button();
            this.but8 = new System.Windows.Forms.Button();
            this.but9 = new System.Windows.Forms.Button();
            this.butO = new System.Windows.Forms.Button();
            this.but00 = new System.Windows.Forms.Button();
            this.but000 = new System.Windows.Forms.Button();
            this.butPlus = new System.Windows.Forms.Button();
            this.butE = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.butMinus = new System.Windows.Forms.Button();
            this.butMultiply = new System.Windows.Forms.Button();
            this.butDevide = new System.Windows.Forms.Button();
            this.butSQRT = new System.Windows.Forms.Button();
            this.butC = new System.Windows.Forms.Button();
            this.butAdHoc = new System.Windows.Forms.Button();
            this.butPercent = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // but1
            // 
            this.but1.Location = new System.Drawing.Point(11, 152);
            this.but1.Name = "but1";
            this.but1.Size = new System.Drawing.Size(44, 30);
            this.but1.TabIndex = 0;
            this.but1.Text = "1";
            this.but1.UseVisualStyleBackColor = true;
            this.but1.Click += new System.EventHandler(this.but1_Click);
            // 
            // but2
            // 
            this.but2.Location = new System.Drawing.Point(61, 152);
            this.but2.Name = "but2";
            this.but2.Size = new System.Drawing.Size(44, 30);
            this.but2.TabIndex = 2;
            this.but2.Text = "2";
            this.but2.UseVisualStyleBackColor = true;
            this.but2.Click += new System.EventHandler(this.but2_Click);
            // 
            // but3
            // 
            this.but3.Location = new System.Drawing.Point(111, 152);
            this.but3.Name = "but3";
            this.but3.Size = new System.Drawing.Size(44, 30);
            this.but3.TabIndex = 3;
            this.but3.Text = "3";
            this.but3.UseVisualStyleBackColor = true;
            this.but3.Click += new System.EventHandler(this.but3_Click);
            // 
            // but4
            // 
            this.but4.Location = new System.Drawing.Point(161, 152);
            this.but4.Name = "but4";
            this.but4.Size = new System.Drawing.Size(44, 30);
            this.but4.TabIndex = 4;
            this.but4.Text = "4";
            this.but4.UseVisualStyleBackColor = true;
            this.but4.Click += new System.EventHandler(this.but4_Click);
            // 
            // but5
            // 
            this.but5.Location = new System.Drawing.Point(11, 188);
            this.but5.Name = "but5";
            this.but5.Size = new System.Drawing.Size(44, 30);
            this.but5.TabIndex = 5;
            this.but5.Text = "5";
            this.but5.UseVisualStyleBackColor = true;
            this.but5.Click += new System.EventHandler(this.but5_Click);
            // 
            // but6
            // 
            this.but6.Location = new System.Drawing.Point(61, 188);
            this.but6.Name = "but6";
            this.but6.Size = new System.Drawing.Size(44, 30);
            this.but6.TabIndex = 6;
            this.but6.Text = "6";
            this.but6.UseVisualStyleBackColor = true;
            this.but6.Click += new System.EventHandler(this.but6_Click);
            // 
            // but7
            // 
            this.but7.Location = new System.Drawing.Point(111, 188);
            this.but7.Name = "but7";
            this.but7.Size = new System.Drawing.Size(44, 30);
            this.but7.TabIndex = 7;
            this.but7.Text = "7";
            this.but7.UseVisualStyleBackColor = true;
            this.but7.Click += new System.EventHandler(this.but7_Click);
            // 
            // but8
            // 
            this.but8.Location = new System.Drawing.Point(161, 188);
            this.but8.Name = "but8";
            this.but8.Size = new System.Drawing.Size(44, 30);
            this.but8.TabIndex = 8;
            this.but8.Text = "8";
            this.but8.UseVisualStyleBackColor = true;
            this.but8.Click += new System.EventHandler(this.but8_Click);
            // 
            // but9
            // 
            this.but9.Location = new System.Drawing.Point(11, 224);
            this.but9.Name = "but9";
            this.but9.Size = new System.Drawing.Size(44, 30);
            this.but9.TabIndex = 9;
            this.but9.Text = "9";
            this.but9.UseVisualStyleBackColor = true;
            this.but9.Click += new System.EventHandler(this.but9_Click);
            // 
            // butO
            // 
            this.butO.Location = new System.Drawing.Point(61, 224);
            this.butO.Name = "butO";
            this.butO.Size = new System.Drawing.Size(44, 30);
            this.butO.TabIndex = 10;
            this.butO.Text = "0";
            this.butO.UseVisualStyleBackColor = true;
            this.butO.Click += new System.EventHandler(this.butO_Click);
            // 
            // but00
            // 
            this.but00.Location = new System.Drawing.Point(111, 224);
            this.but00.Name = "but00";
            this.but00.Size = new System.Drawing.Size(44, 30);
            this.but00.TabIndex = 11;
            this.but00.Text = "00";
            this.but00.UseVisualStyleBackColor = true;
            this.but00.Click += new System.EventHandler(this.but00_Click);
            // 
            // but000
            // 
            this.but000.Location = new System.Drawing.Point(161, 224);
            this.but000.Name = "but000";
            this.but000.Size = new System.Drawing.Size(44, 30);
            this.but000.TabIndex = 12;
            this.but000.Text = "000";
            this.but000.UseVisualStyleBackColor = true;
            this.but000.Click += new System.EventHandler(this.but000_Click);
            // 
            // butPlus
            // 
            this.butPlus.Location = new System.Drawing.Point(16, 362);
            this.butPlus.Name = "butPlus";
            this.butPlus.Size = new System.Drawing.Size(67, 45);
            this.butPlus.TabIndex = 13;
            this.butPlus.Text = "+";
            this.butPlus.UseVisualStyleBackColor = true;
            this.butPlus.Click += new System.EventHandler(this.butPlus_Click);
            // 
            // butE
            // 
            this.butE.Location = new System.Drawing.Point(226, 310);
            this.butE.Name = "butE";
            this.butE.Size = new System.Drawing.Size(75, 97);
            this.butE.TabIndex = 14;
            this.butE.Text = "=";
            this.butE.UseVisualStyleBackColor = true;
            this.butE.Click += new System.EventHandler(this.butE_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 111);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 20);
            this.label1.TabIndex = 15;
            this.label1.Text = "label1";
            // 
            // butMinus
            // 
            this.butMinus.Location = new System.Drawing.Point(89, 362);
            this.butMinus.Name = "butMinus";
            this.butMinus.Size = new System.Drawing.Size(67, 45);
            this.butMinus.TabIndex = 16;
            this.butMinus.Text = "-";
            this.butMinus.UseVisualStyleBackColor = true;
            this.butMinus.Click += new System.EventHandler(this.butMinus_Click);
            // 
            // butMultiply
            // 
            this.butMultiply.Location = new System.Drawing.Point(16, 310);
            this.butMultiply.Name = "butMultiply";
            this.butMultiply.Size = new System.Drawing.Size(67, 46);
            this.butMultiply.TabIndex = 17;
            this.butMultiply.Text = "*";
            this.butMultiply.UseVisualStyleBackColor = true;
            this.butMultiply.Click += new System.EventHandler(this.butMultiply_Click);
            // 
            // butDevide
            // 
            this.butDevide.Location = new System.Drawing.Point(89, 310);
            this.butDevide.Name = "butDevide";
            this.butDevide.Size = new System.Drawing.Size(67, 46);
            this.butDevide.TabIndex = 18;
            this.butDevide.Text = "/";
            this.butDevide.UseVisualStyleBackColor = true;
            this.butDevide.Click += new System.EventHandler(this.butDevide_Click);
            // 
            // butSQRT
            // 
            this.butSQRT.Location = new System.Drawing.Point(162, 310);
            this.butSQRT.Name = "butSQRT";
            this.butSQRT.Size = new System.Drawing.Size(58, 46);
            this.butSQRT.TabIndex = 19;
            this.butSQRT.Text = "√";
            this.butSQRT.UseVisualStyleBackColor = true;
            this.butSQRT.Click += new System.EventHandler(this.butSQRT_Click);
            // 
            // butC
            // 
            this.butC.Location = new System.Drawing.Point(225, 152);
            this.butC.Name = "butC";
            this.butC.Size = new System.Drawing.Size(75, 66);
            this.butC.TabIndex = 20;
            this.butC.Text = "C";
            this.butC.UseVisualStyleBackColor = true;
            this.butC.Click += new System.EventHandler(this.butC_Click);
            // 
            // butAdHoc
            // 
            this.butAdHoc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.butAdHoc.Location = new System.Drawing.Point(225, 224);
            this.butAdHoc.Name = "butAdHoc";
            this.butAdHoc.Padding = new System.Windows.Forms.Padding(4, 0, 0, 4);
            this.butAdHoc.Size = new System.Drawing.Size(75, 30);
            this.butAdHoc.TabIndex = 21;
            this.butAdHoc.Text = "test";
            this.butAdHoc.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.butAdHoc.UseVisualStyleBackColor = false;
            this.butAdHoc.Click += new System.EventHandler(this.butAdHoc_Click);
            // 
            // butPercent
            // 
            this.butPercent.Location = new System.Drawing.Point(162, 362);
            this.butPercent.Name = "butPercent";
            this.butPercent.Size = new System.Drawing.Size(58, 45);
            this.butPercent.TabIndex = 22;
            this.butPercent.Text = "%";
            this.butPercent.UseVisualStyleBackColor = true;
            this.butPercent.Click += new System.EventHandler(this.butPercent_Click);
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.textBox1.BorderStyle = global::CalCool.Properties.Settings.Default.Bord;
            this.textBox1.DataBindings.Add(new System.Windows.Forms.Binding("BorderStyle", global::CalCool.Properties.Settings.Default, "Bord", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged, null, "N3"));
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(12, 12);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.RightToLeft = global::CalCool.Properties.Settings.Default.Cal;
            this.textBox1.Size = new System.Drawing.Size(288, 69);
            this.textBox1.TabIndex = 1;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // CalCool
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(312, 418);
            this.Controls.Add(this.butPercent);
            this.Controls.Add(this.butAdHoc);
            this.Controls.Add(this.butC);
            this.Controls.Add(this.butSQRT);
            this.Controls.Add(this.butDevide);
            this.Controls.Add(this.butMultiply);
            this.Controls.Add(this.butMinus);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.butE);
            this.Controls.Add(this.butPlus);
            this.Controls.Add(this.but000);
            this.Controls.Add(this.but00);
            this.Controls.Add(this.butO);
            this.Controls.Add(this.but9);
            this.Controls.Add(this.but8);
            this.Controls.Add(this.but7);
            this.Controls.Add(this.but6);
            this.Controls.Add(this.but5);
            this.Controls.Add(this.but4);
            this.Controls.Add(this.but3);
            this.Controls.Add(this.but2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.but1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "CalCool";
            this.Text = "CalCool";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button but1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button but2;
        private System.Windows.Forms.Button but3;
        private System.Windows.Forms.Button but4;
        private System.Windows.Forms.Button but5;
        private System.Windows.Forms.Button but6;
        private System.Windows.Forms.Button but7;
        private System.Windows.Forms.Button but8;
        private System.Windows.Forms.Button but9;
        private System.Windows.Forms.Button butO;
        private System.Windows.Forms.Button but00;
        private System.Windows.Forms.Button but000;
        private System.Windows.Forms.Button butPlus;
        private System.Windows.Forms.Button butE;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button butMinus;
        private System.Windows.Forms.Button butMultiply;
        private System.Windows.Forms.Button butDevide;
        private System.Windows.Forms.Button butSQRT;
        private System.Windows.Forms.Button butC;
        private System.Windows.Forms.Button butAdHoc;
        private System.Windows.Forms.Button butPercent;
        // foo
    }
}


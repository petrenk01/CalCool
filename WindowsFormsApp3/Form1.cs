﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CalCool
{
    public partial class CalCool : Form
    {
        public CalCool()
        {
            InitializeComponent();
        }

        private void but1_Click(object sender, EventArgs e)
        {
            textBox1.Text += "1"; 
        }

        private void but2_Click(object sender, EventArgs e)
        {
            textBox1.Text += "2";
        }

        private void but3_Click(object sender, EventArgs e)
        {
            textBox1.Text += "3";
        }

        private void but4_Click(object sender, EventArgs e)
        {
            textBox1.Text += "4";
        }

        private void but5_Click(object sender, EventArgs e)
        {
            textBox1.Text += "5";
        }

        private void but6_Click(object sender, EventArgs e)
        {
            textBox1.Text += "6";
        }

        private void but7_Click(object sender, EventArgs e)
        {
            textBox1.Text += "7";
        }

        private void but8_Click(object sender, EventArgs e)
        {
            textBox1.Text += "8";
        }

        private void but9_Click(object sender, EventArgs e)
        {
            textBox1.Text += "9";
        }

        private void butO_Click(object sender, EventArgs e)
        {
            textBox1.Text += "0";
        }

        private void but00_Click(object sender, EventArgs e)
        {
            textBox1.Text += "00";
        }

        private void but000_Click(object sender, EventArgs e)
        {
            textBox1.Text += "000";
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            label1.Text = "hi";
        }
        private void readVal()
        {
            double t = 0;
            double.TryParse(textBox1.Text, out t);
            tempVal = t;
            textBox1.Text = "";
        }

        private void butPlus_Click(object sender, EventArgs e)
        {
             readVal();
            op ="+";
        }
        double tempVal = 0;
        string op = "";
        private void butE_Click(object sender, EventArgs e)
        {
            double t = 0;
            double r = 0;
            double.TryParse(textBox1.Text, out t);

            switch (op)
            {
                case "+":
                    r = t + tempVal;
                    break;
                case "-":
                    r = tempVal - t;
                    break;
                case "/":
                    if (t == 0)
                    {
                        MessageBox.Show("Debil, go back to school");
                        break;
                    }
                    r = tempVal / t;
                    break;
                case "*":
                    r = tempVal * t;
                    break;
                case "√":
                    r = Math.Sqrt(t);
                    break;
                //case "%":
                //    r = tempVal * t / 100;
                //    break;
                default:
                    MessageBox.Show($"An unexpected operator ({op})");
                    break;
            }//operators(+-*/)


            textBox1.Text = r.ToString();
        }
        string tempValue = "";
      

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (textBox1.Text == "")
                return;
            double tt = 0;
            bool isConverted = double.TryParse(textBox1.Text, out tt); 
            if (isConverted)
            {
                tempValue = tt.ToString();
            }
            else
            {
                textBox1.Text = tempValue;
            }
        }

        private void butMinus_Click(object sender, EventArgs e)
        {
            readVal();
            op = "-";
        }

        private void butMultiply_Click(object sender, EventArgs e)
        {
            readVal();
            op = "*";
        }

        private void butDevide_Click(object sender, EventArgs e)
        {
            readVal();
            op = "/";
        }
        // Finds the square root of a positive number  
        private double Power(double a, int b)
        {
            if (b < 0)
            {
                throw new ApplicationException("B must be a positive integer or zero");
            }
            if (b == 0) return 1;
            if (a == 0) return 0;
            if (b % 2 == 0)
            {
                return Power(a * a, b / 2);
            }
            else if (b % 2 == 1)
            {
                return a * Power(a * a, b / 2);
            }
            return 0;
        } // end sqrt()  


        private void butSQRT_Click(object sender, EventArgs e)
        {
            double tt = 0;
            bool isConverted = double.TryParse(textBox1.Text, out tt);
            if ((!isConverted) || (tt < 0))
            {
                label1.Text= "moron";
                return;
            }

            tt= Math.Sqrt(tt);
            textBox1.Text = tt.ToString();
        }

        private void butC_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            label1.Text = "";
        }

        private void butAdHoc_Click(object sender, EventArgs e)
        {
            var array1 = new[] { 1, 3, 5, 7, 8, 2, 4, 7, 3245, 2, 4, 237, 343456, 3, 6, 5, 5 };
            var array2 = array1.Select(x => x * 2).ToList();
            var dictionary = new Dictionary<string, string>();
            dictionary.Add("apple", "aaaaaaaaaaaaaaaaaaaaaasdasds");
            dictionary.Add("anon", "annnonnonon");
            dictionary.Add("peach", "peaaaaaaaaaaaach");
            dictionary.Add("gavno", "gggggggggggggg");
            var dd = dictionary.Where(x => x.Key.StartsWith("a"));
            dictionary.Add("melon", "sdfg");

            string str  = "easiest way is";
            foreach (char c in str)
            {
                label1.Text += c;
            }
        }

        private void butPercent_Click(object sender, EventArgs e)
        {
            double p = 0;
            double t = 0; // 5%
            // tempVal <-100%

            t = double.Parse(textBox1.Text); 
            p = tempVal / 100 * t;
            p = Math.Round(p,3);
            textBox1.Text = p.ToString();
        }
 
    }
}
 
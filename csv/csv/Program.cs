﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;

namespace csv
{
    class Program
    {
        static void Main(string[] args)
        {
            //var dataTime1 = DateTime.Now;
            //var dtStr = dataTime1.AddYears(-2).ToString();
            //var goodDate = "8/21/2015 10:04:54 PM";
            //var badDate = "8/21/201510:04:54 PM";
            //DateTime d1, d2;
            //DateTime.TryParse(goodDate, out d1);
            //DateTime.TryParse(badDate, out d2);

            var csvFile = @"C:\git\edik\CalCool\csv\csv\BankMarketing.csv";
            //var reader = new CsvHelper.CsvReader()
            Console.WriteLine(csvFile);
            var foo = new List<string>();
            var bankMarketingRecods = new List<BankMarketing>();
           
            var sr = new StreamReader(csvFile);
            
            var headers = sr.ReadLine();
            while(!sr.EndOfStream)
            {
                var line = sr.ReadLine();
                var bankMarketing = LineToBankMarketing(line);
                if (bankMarketing != null)
                {
                    bankMarketingRecods.Add(bankMarketing);
                }
            }
            var highBalance = bankMarketingRecods.Where(x => x.Balance>5000).ToList();
            var avarageAgeHighPerformers = highBalance.Select(x => x.Age).Average();
            var holost = bankMarketingRecods.Where(x => !string.Equals(x.Marital, "married", StringComparison.CurrentCultureIgnoreCase)).ToList();
        }

        static BankMarketing LineToBankMarketing(string line)
        {
            try
            {
                var cells = line.Split(',');
                var bankMarketing = new BankMarketing
                {
                    Age = Convert.ToInt32(cells[0]),
                    Job = cells[1],
                    Marital = cells[2],
                    Education = cells[3],
                    Default = cells[4],
                    Balance = Convert.ToInt32(cells[5]),
                    HousingLoan = cells[6],
                    PersonalLoan = cells[7],
                    Duration = Convert.ToInt32(cells[8]),
                    Campaign = Convert.ToInt32(cells[9]),
                    TermDeposit = cells[10]
                };
                return bankMarketing;
            }
            catch (Exception e)
            {
                Console.WriteLine($"LineToBankMarketing threw an error {e.Message}, line was:{line}");
                return null;
            }
        }
    }

    public class BankMarketing
    {
        public int Age { get; set; }
        public string Job { get; set; }
        public string Marital { get; set; }
        public string Education { get; set; }
        public string Default { get; set; }
        public int Balance { get; set; }
        public string HousingLoan { get; set; }
        public string PersonalLoan { get; set; }
        public int Duration { get; set; }
        public int Campaign { get; set; }
        public string TermDeposit { get; set; }  
    }
}
